import { Button } from '@/components/ui/button';
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue} from '@/components/ui/select';
//import {SelectTrigger} from "../components/ui/select";

export default function Home() {
	return (
		<main className="p-10" >
			<Button variant="warning" align="center" >Label</Button>
			<Button variant="primary" size="medium">Label</Button>
			<Button variant="danger">Label</Button>
			<Button variant="ghost">Label</Button>
			<Select>
				Select Your Variant
				<SelectTrigger className="w-[280px]">
					<SelectValue placeholder="Theme" />
				</SelectTrigger>
				<SelectContent position="popper">
					<SelectItem value="light">Light</SelectItem>
					<SelectItem value="dark">Dark</SelectItem>
					<SelectItem value="system">System</SelectItem>
					<SelectItem value="cringe">Cringe</SelectItem>
				</SelectContent>
			</Select>
		</main>
	)
}
